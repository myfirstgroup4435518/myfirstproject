import React from "react"
const SearchBtn = ({searchQuery,setSearchQuery}) =>{
    return(
        <div className="container search create-item-container" >
            <div>
                <input type="text" placeholder="Введите модель"  value={searchQuery} onChange={(event)=>setSearchQuery(event.target.value)}/>
                <button>Найти </button>
            </div>            
        </div>
    )
}
export default SearchBtn