import { useEffect, useState } from "react"
import React from "react"
const AddForm = ({phones,addPhone,selectedPhone,setPhones}) => {
    //Состояние для хранения ввода модели телефона
    const [model,setModel] = useState('')
    //Состояние для хранения описания модели телефона
    const [description,setDescription] = useState('')
    //Состояние для хранения ввода цены телефона
    const [price,setPrice] = useState('')
    //Состояние для хранения выбранного файла
    const [selectedFile,setSelectedFile]= useState(null)
    //Состояние для вывода ошибки
    const [error,setError] = useState(null)
    //Функции для считывания ввода пользователя
    const userModel = (event)=>{
        event.preventDefault()
        setModel(event.target.value)
    }
    const userDescription = (event)=>{
        event.preventDefault()
        setDescription(event.target.value)
    }
    const userPrice = (event)=>{
        event.preventDefault()
        setPrice(event.target.value)
    }
    const handleFileInputChange = (event) => {
        const file = event.target.files[0]
        const reader = new FileReader()

        reader.onloadend = () => {
            setSelectedFile(reader.result)
        }

        reader.readAsDataURL(file)
    }

    useEffect(()=>{
        if (selectedPhone){
            setModel(selectedPhone.model)
            setDescription(selectedPhone.description)
            setPrice(selectedPhone.price)
            setSelectedFile(selectedPhone.image)
        }else{
            setModel('')
            setDescription('')
            setPrice('')
            setSelectedFile(null)
        }
    },[selectedPhone])

    const handleSubmit = (event)=>{
        event.preventDefault()
        if (model.trim() === ""){
            setError("Пожалуйста введите модель телефона")
            return
        }
        if (description.trim() === ""){
            setError("Пожалуйста введите описание телефона")
            return
        }
        if (price.trim() === "" || price<0 || isNaN(price) || parseFloat(price)<=0){
            setError("Проверьте корректность данных в поле Price")
            return
        }
        if (!selectedFile){
            setError('Пожалуйста прикрепите фото')
            return
        }
        addPhone(model,description,price,selectedFile)
        setModel('')
        setDescription('')
        setPrice('')
        setSelectedFile(null)
        setError(null)
        event.target.closest('form').reset()
    }
    
    return  (
        <form className="addform" >
            <p>Модель:</p>
            <input type="text" placeholder="Введите модель" name="model" onChange={userModel} value={model}/>
            <p>Описание:</p>
            <input type="text" placeholder="Введите описание" name="description" onChange={userDescription} value={description}/>
            <p>Цена:</p>
            <input type="text" placeholder="Введите цену за товар" name="price" onChange={userPrice} value={price}/>
            <input type="file" accept="image/*"  onChange={handleFileInputChange} />
            {error && <p>{error}</p>}
            <button onClick={handleSubmit}>{selectedPhone?'Сохранить':'Добавить'}</button>
        </form>
    )
}
export default AddForm