import { useEffect, useState } from 'react';
import './App.css';
import StoreNav from './components/StoreNav';
import React from 'react';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import AdminForm from './pages/AdminForm';
import CreateItem from './pages/CreateItem';
import BasketPage from './pages/BasketPage';
import AboutItem from './pages/AboutItem';

function Home() {
  //Создаю отдельное состояние где буду хранить подтвержденные созданные записи во вкладке главная
  const [confirmedPhones,setConfirmedPhones] = useState([])
  //состояние для того чтобы отображать телефоны,в которых мы нажали добавить в корзину,чтобы потом отображать в basketPage
  const [selectedPhone, setSelectedPhone] = useState([]);
  useEffect(() => {
    const savedPhones = localStorage.getItem('confirmedPhones');
    console.log('saved1:',savedPhones)
    if (savedPhones) {
      setConfirmedPhones(JSON.parse(savedPhones));
    }
  }, []); 
  useEffect(() => {
    localStorage.setItem('confirmedPhones', JSON.stringify(confirmedPhones));
  }, [confirmedPhones]);
  useEffect(() => {
    const savedPhones = localStorage.getItem('selectedPhone');
    if (savedPhones) {
      setSelectedPhone(JSON.parse(savedPhones));
    }
  }, []); 
  useEffect(() => {
    localStorage.setItem('selectedPhone', JSON.stringify(selectedPhone));
  }, [selectedPhone]);
  return (
    <Router>
      <div className="Home" >
        <div>
          <h1 className='logo'>Мини интернет-магазин</h1>
          <StoreNav confirmedPhones={confirmedPhones}/>
        </div>
        <div className="routes-container">
        <Routes>
          <Route path="/" element={<AdminForm confirmedPhones={confirmedPhones} setConfirmedPhones={setConfirmedPhones}/>}/>
          <Route path="/products" element={<CreateItem confirmedPhones={confirmedPhones} setConfirmedPhones={setConfirmedPhones}/>} />
          <Route path="/basket/" element={<BasketPage  selectedPhone={selectedPhone} setSelectedPhone={setSelectedPhone}/>}/>
          <Route path="/aboutitem/:phoneId" element={<AboutItem confirmedPhones={confirmedPhones} selectedPhone={selectedPhone} setSelectedPhone={setSelectedPhone}/>}/>
        </Routes>
        </div>
        <div className="footer" ></div>
      </div>
     </Router>
  );
}

export default Home;
