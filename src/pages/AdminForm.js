import AddForm from "../components/AddForm"
import PhoneItem from "../components/PhoneItem"
// import SearchBtn from "../components/SearchBtn"
import React, { useEffect } from "react"
import { useState } from "react"
const AdminForm = ({confirmedPhones,setConfirmedPhones}) => {
  //Состояние где буду хранить все телефоны
  const [phones,setPhones] = useState([])
  //состояние ,чтобы понять редактируется элемент или нет
  const [editMode,SetEditMode] = useState(false)
  //Состояние для выбранного для редактирования телефона(телефон, который мы будем редактировать)
  const [selectedPhone,SetSelectedPhone] = useState(null)
  //Создаю функцию для добавления телефона
  const addPhone = (model,description,price,image) => {
    if (editMode && selectedPhone){
      const updatePhone ={
        id: selectedPhone.id,
        model: model,
        description: description,
        price: price,
        image: image
      }
      setPhones([...phones.filter(phone => phone.id !== selectedPhone.id), updatePhone])
      SetEditMode(false)
      SetSelectedPhone(null)
    } else{
    const newPhone = {
      id: Date.now(),
      model: model,
      description: description,
      price: price,
      image: image
    }
    setPhones([...phones,newPhone])
    }
  }
  //Создаю функцию,которая будет, которая при клике на галочку будет удалять Phone с текущего массива и переносить этот элемент в новый массив для вкладки главная(подтвержденные записи) 
  const confirmPhone = (phone) => {
    setConfirmedPhones((prevConfirmedPhones) => [...prevConfirmedPhones, phone]);
    setPhones((prevPhones) => prevPhones.filter((p) => p.id !== phone.id));
  };
  
  //Создаю функцию для редактирования телефона
  const editPhone = (phone) => {
    SetSelectedPhone(phone)
    SetEditMode(true)
  }
  //Создаю функцию для удаления телефона
  const deletePhone = (id) => {
    setPhones([...phones.filter((phone)=>phone.id!==id)])
    
  }

  useEffect(() => {
    const savedPhones = localStorage.getItem("phones");
    if (savedPhones) {
      setPhones(JSON.parse(savedPhones));
    }
  }, []);

  useEffect(() => {
    localStorage.setItem("phones", JSON.stringify(phones));
  }, [phones]);

    return(
        <>
          <div className="container header">
            <AddForm phones={phones} setPhones={setPhones} addPhone={addPhone} selectedPhone={selectedPhone} SetSelectedPhone={SetSelectedPhone} setPhones={setPhones} SetEditMode={SetEditMode}/>
          </div>
          
          <div className='container phones'>
            {phones
                 .map((phone)=>{ 
                    return <PhoneItem phone={phone} key={phone.id} deletePhone={deletePhone} editPhone={editPhone} confirmPhone={confirmPhone} setConfirmedPhones={setConfirmedPhones}/>
                })}
            </div> 
        </>
    )
}
export default AdminForm